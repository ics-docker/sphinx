FROM registry.esss.lu.se/ics-docker/miniconda:4.8.4

ENV SPHINX_VERSION 3.2.1
RUN conda create -y -n sphinx -c conda-forge \
  make \
  sphinx="${SPHINX_VERSION}" \
  myst-parser \
  sphinx_rtd_theme \
  && pip install --no-cache-dir sphinxcontrib-blockdiag blockdiag \
  && conda clean -ay

ENV PATH /opt/conda/envs/sphinx/bin:$PATH
