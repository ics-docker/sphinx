# sphinx docker image

[Docker](https://www.docker.com) image to build [Sphinx](http://www.sphinx-doc.org/) documentation.

The image includes:

- make
- sphinx
- sphinxcontrib
- sphinxcontrib-blockdiag
- sphinx_rtd_theme
- myst-parser


## How to use this image

Assuming you created some documentation in the `docs` directory using `sphinx-quickstart`,
here a .gitlab-ci.yml example that you could use::

    pages:
      tags:
        - docker
      stage: deploy
      image: registry.esss.lu.se/ics-docker/sphinx:latest
      script:
        - sphinx-build -M html docs docs/_build
        - mv docs/_build/html public
      artifacts:
        paths:
          - public
      only:
        - tags

Note that if some extra dependencies are required to build your project, you can install them using `conda` or `pip`
using `before_script` or just before the `sphinx-build` command.
If those dependencies are really common, they can be added to this image.

The `sphinx-build -M html docs docs/_build` command could be replaced by `make -C docs html`.
